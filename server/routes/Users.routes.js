
const express = require('express');
const router = express.Router();



const checkAuth = require('../../moddleware/Auth');


/************************************
 * ADMIN CONTROLLERS
 ***********************************/
const AdminloginControllers= require('../controllers/Users.controller');



/**************************************************************
 * @ROUTE       - /admin/signup
 * @METHOD      - POST
***************************************************************/
router.post('/signup', AdminloginControllers.usersignuppost);


/**************************************************************
 * @ROUTE       - /admin/login
 * @METHOD      - POST
***************************************************************/
router.post("/login", checkAuth,AdminloginControllers.userloginpost);



/***************************************************************
 * Users middleware 
 ***************************************************************/
module.exports = router;
