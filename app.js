const express = require('express');
const app = express();
const bodyParser = require('body-parser');
require('dotenv').config()
const config = require('config');
const jwt =require('jsonwebtoken')

const dbconnection = require('./DbConnect/dbconnection');

if (!config.get('jwtPrivateKey')) {
    console.error('FATAL ERROR: jwtPrivateKey is not defined.');
    process.exit(1);
  }


/************************************
 * @DESC    - PARSER JSON BODY
 * @PACKAGE - body-parser
 ***********************************/
app.use( bodyParser.urlencoded({ extended : false }) );
app.use( bodyParser.json() );


/************************************
 * @DESC    - DATABASE CONFIGURATION
 * @PACKAGE - mongoose
 ***********************************/
dbconnection();
app.use(express.json({ extended: false}));


// Blog contant Router and express add
const BlogRouter =require('./server/routes/Blog.routes');
const AuthRouter =require('./server/routes/Auth.routes');
const LoginAdminRouter = require('./server/routes/Users.routes');

/************************************
 * @DESC    -  ROUTER
 * @PACKAGE -  EXPRESS
 ***********************************/


// middleware for apiRoutes
app.use(express.json());
app.use('/admin/blog',BlogRouter);
app.use('/admin',AuthRouter);
app.use('/admin',LoginAdminRouter);






app.use((req, res, next) =>{
    const error = new Error('Not found');
    error.status=404;
    next(error); 
})

app.use((error, req, res, next) =>{
res. status(error.status ||500);
res.json({
    error:{
        massage:error.massage
    }
});

});
// // api test
// (req, res, next) =>{
//    res.status(200).json({
//        massage:'its working!!!!'
//     });
// });



const port = process.env.PORT || 3000;
require('dotenv').config();
app.get('/', (req, res) => {
    res.send(process.env.PORT);
})
app.listen(port, () => {
    console.log(`Server is running on port ${port}.`)
})